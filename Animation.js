export default class Animation {
  currentIndex = 0;
  frames = [];
  now = 0;
  time = 1;
  playing = true;

  constructor(time, offset, frames) {
    this.time = time
    this.currentIndex = offset;
    this.frames = frames;
  }

  setPlaying(playing) {
    this.playing = playing;
  }

  toggle() {
    this.playing = !this.playing;
  }

  nextFrame() {
    ++this.currentIndex;

    if (this.currentIndex >= this.frames.length) {
      this.currentIndex = 0;
    }

    return this.currentIndex;
  }

  getFrame(newTime, lag) {
    if (newTime - this.now > this.time) {
      this.nextFrame();
      this.now = newTime;
    }

    return this.frames[this.currentIndex];
  }
}
