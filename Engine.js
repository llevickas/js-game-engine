import GameObject from './GameObject.js';

export default class Engine {
  updatables = [];
  running = false;
  frameRateHTML = document.createElement('p');
  time = 0;
  frames = 0;
  canvas = document.createElement('canvas');
  ctx = this.canvas.getContext('2d');
  bodyRect = document.body.getClientRects()[0];
  wantedRate = 60;

  constructor() {
    window.addEventListener('resize', () => {
      this.bodyRect = document.body.getClientRects()[0];
      this.canvas.width = this.bodyRect.width;
      this.canvas.height = this.bodyRect.height;
    });
    document.body.append(this.canvas);
    document.body.append(this.frameRateHTML);

    this.frameRateHTML.classList.add('engine-fps');
    this.canvas.classList.add('engine-canvas');
  };

  start() {
    this.running = true;
    this.now = performance.now();
    this.canvas.width = this.bodyRect.width;
    this.canvas.height = this.bodyRect.height;
    this.ctx.fillStyle = 'black';
    this.update();
  }

  stop() {
    this.running = false;
  }

  add(obj) {
    this.updatables.push(obj);
  }

  remove(obj) {
    this.updatables = this.updatables.filter(i => i !== obj);
    obj.html.remove();
  }

  update = () => {
    if (!this.running) {
      return null;
    }

    const now = performance.now();
    const diff = (now - this.time) / 1000;
    const frameRate = Math.ceil(1 / diff);
    const lag = this.wantedRate / frameRate;

    this.ctx.clearRect(0, 0, this.bodyRect.width, this.bodyRect.height);
    this.time = now;
    this.frameRateHTML.textContent = frameRate;
    this.ctx.fillRect(0, 0, this.bodyRect.width, this.bodyRect.height);

    for (let updatable of this.updatables) {
      updatable.update(lag);
      updatable.draw(this.ctx, now, lag);
    }

    requestAnimationFrame(this.update);
  }
};
