export default class GameObject {
  image = new Image();
  speed = 1;
  left = 0;
  top = 0;
  keys = null;

  setSprite(src) {
    this.image.src = src;
  }

  constructor(x, y, speed, keys) {
    if (typeof x === 'number') {
      this.left = x;
    }

    if (typeof y === 'number') {
      this.top = y;
    }

    if (typeof speed === 'number') {
      this.speed = speed;
    }

    if (typeof keys === 'object') {
      this.keys = keys;
      document.addEventListener('keydown', (event) => this.keys[event.keyCode] = true);
      document.addEventListener('keyup', (event) => this.keys[event.keyCode] = false);
    }
  }

  draw(ctx) {}

  update(interpolation) {}
}
