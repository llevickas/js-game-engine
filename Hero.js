import GameObject from './GameObject.js';
import Animation from './Animation.js';

export default class Hero extends GameObject {
  currentState = 'idle';
  scale = {x: 1, y: 1};

  animations = {
    idle: new Animation(40, 0, [
      './sprites/hero/Idle__000.png',
      './sprites/hero/Idle__001.png',
      './sprites/hero/Idle__002.png',
      './sprites/hero/Idle__003.png',
      './sprites/hero/Idle__004.png',
      './sprites/hero/Idle__005.png',
      './sprites/hero/Idle__006.png',
      './sprites/hero/Idle__007.png',
      './sprites/hero/Idle__008.png',
      './sprites/hero/Idle__009.png',
    ]),
    run: new Animation(40, 0, [
      './sprites/hero/Run__000.png',
      './sprites/hero/Run__001.png',
      './sprites/hero/Run__002.png',
      './sprites/hero/Run__003.png',
      './sprites/hero/Run__004.png',
      './sprites/hero/Run__005.png',
      './sprites/hero/Run__006.png',
      './sprites/hero/Run__007.png',
      './sprites/hero/Run__008.png',
      './sprites/hero/Run__009.png',
    ]),
    attack: new Animation(10, 0, [
      './sprites/hero/Attack__000.png',
      './sprites/hero/Attack__001.png',
      './sprites/hero/Attack__002.png',
      './sprites/hero/Attack__003.png',
      './sprites/hero/Attack__004.png',
      './sprites/hero/Attack__005.png',
      './sprites/hero/Attack__006.png',
      './sprites/hero/Attack__007.png',
      './sprites/hero/Attack__008.png',
      './sprites/hero/Attack__009.png',
    ]),
  };

  constructor(x, y, speed) {
    super(x, y, speed, {69: false, 65: false});
  }

  update(lag) {
    const keysPressed = Object.values(this.keys).filter(k => k === true).length;
    const speed = Math.ceil((this.speed * lag) / keysPressed);

    let newLeft = this.left;
    let newTop = this.top;

    if (this.keys[65]) {
      newLeft -= speed;
      this.scale.x = -1;
    }

    if (this.keys[69]) {
      newLeft += speed;
      this.scale.x = 1;
    }

    if (this.keys[32]) {
      this.currentState = 'attack';
    } else if ((this.keys[69] || this.keys[65]) && newLeft !== this.left) {
      this.currentState = 'run';
    } else {
      this.currentState = 'idle';
    }

    this.left = newLeft;
    this.top = newTop;
  }

  draw(ctx, time, lag) {
    this.setSprite(this.animations[this.currentState].getFrame(time, lag));
    ctx.save();

    if (this.scale.x < 0) {
      if (this.currentState === 'attack') {
        ctx.translate(this.image.width * 0.25, 0);
      } else if (this.currentState === 'run') {
        ctx.translate(this.image.width * 0.5, 0);
      } else {
        ctx.translate(this.image.width * 0.5, 0);
      }

      ctx.scale(this.scale.x, 1);
    }

    ctx.drawImage(
      this.image,
      0, 0,
      this.image.width, this.image.height,
      this.left * this.scale.x, this.top,
      this.image.width * 0.5, this.image.height * 0.5
    );

    ctx.restore();
  }
}
