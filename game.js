import Engine from './Engine.js';
import GameObject from './GameObject.js';
import Hero from './Hero.js';

document.addEventListener('DOMContentLoaded', function() {
  const engine = new Engine();
  engine.start();
  engine.add(new Hero(10, 15, 10));
});
